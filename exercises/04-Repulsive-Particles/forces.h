
/* compute repulsive forces for N particles in 3D */
void compute_forces(int N, const double *pos, double *forces);

