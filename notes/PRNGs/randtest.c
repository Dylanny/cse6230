#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <float.h>
#if defined(_OPENMP)
#include <omp.h>
#endif

/* After Hager & Wellein, Listing 1.2 */
static double get_walltime(void)
{
  struct timeval tp;
  gettimeofday(&tp, NULL);

  return (double) (tp.tv_sec + tp.tv_usec/1.e6);
}

int main(void)
{
  int N = 1000000;
  int NTIMES = 10;
  int i;
  double min_time[4] = {DBL_MAX,DBL_MAX,DBL_MAX,DBL_MAX},
         max_time[4] = {0.}, avg_time[4] = {0.};

  for (i = 0; i < NTIMES; i++) {
    double global_avg, time;

    time = -get_walltime();
    global_avg = 0.;
    #pragma omp parallel
    {
      int    j;
      double my_avg = 0.;
      #pragma omp for
      for (j = 0; j < N; j++) {
        my_avg += (double) rand() / RAND_MAX;
      }
      #pragma omp atomic
      global_avg += my_avg;
    }
    global_avg /= N;
    time += get_walltime();
    if (i) {
      min_time[0] = time < min_time[0] ? time : min_time[0];
      max_time[0] = time > max_time[0] ? time : max_time[0];
      avg_time[0] += time;
    }

    time = -get_walltime();
    global_avg = 0.;
    #pragma omp parallel
    {
      int          j;
      double       my_avg = 0.;
      unsigned int seed = 0;
#if defined(_OPENMP)
      seed = omp_get_thread_num();
#endif
      #pragma omp for
      for (j = 0; j < N; j++) {
        my_avg += (double) rand_r(&seed) / RAND_MAX;
      }
      #pragma omp atomic
      global_avg += my_avg;
    }
    global_avg /= N;
    time += get_walltime();
    if (i) {
      min_time[1] = time < min_time[1] ? time : min_time[1];
      max_time[1] = time > max_time[1] ? time : max_time[1];
      avg_time[1] += time;
    }

    time = -get_walltime();
    global_avg = 0.;
    #pragma omp parallel
    {
      int    j;
      double my_avg = 0.;
      #pragma omp for
      for (j = 0; j < N; j++) {
        my_avg += drand48();
      }
      #pragma omp atomic
      global_avg += my_avg;
    }
    global_avg /= N;
    time += get_walltime();
    if (i) {
      min_time[2] = time < min_time[2] ? time : min_time[2];
      max_time[2] = time > max_time[2] ? time : max_time[2];
      avg_time[2] += time;
    }

    time = -get_walltime();
    global_avg = 0.;
    #pragma omp parallel
    {
      int    j;
      double my_avg = 0.;
      unsigned short seed[3] = {0};
#if defined(_OPENMP)
      seed[0] = omp_get_thread_num();
#endif
      #pragma omp for
      for (j = 0; j < N; j++) {
        my_avg += erand48(seed);
      }
      #pragma omp atomic
      global_avg += my_avg;
    }
    global_avg /= N;
    time += get_walltime();
    if (i) {
      min_time[3] = time < min_time[3] ? time : min_time[3];
      max_time[3] = time > max_time[3] ? time : max_time[3];
      avg_time[3] += time;
    }
  }
  for (i = 0; i < 4; i++) avg_time[i] /= (NTIMES - 1);
  printf ("Time for %d rand() calls: %f [%f, %f]\n",N,avg_time[0],min_time[0],max_time[0]);
  printf ("Time for %d rand_r() calls: %f [%f, %f]\n",N,avg_time[1],min_time[1],max_time[1]);
  printf ("Time for %d drand48() calls: %f [%f, %f]\n",N,avg_time[2],min_time[2],max_time[2]);
  printf ("Time for %d erand48() calls: %f [%f, %f]\n",N,avg_time[3],min_time[3],max_time[3]);
  return 0;
}
